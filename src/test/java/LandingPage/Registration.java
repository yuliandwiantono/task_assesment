package LandingPage;

import LandingPage.Steps.ButtonLoginSteps;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class Registration extends SerenityStory {
    @Steps
    Registration RegistrationStep;

    //Success Registration//

    @Given("I want registration for web")
    public void givenIwatRegistrationWeb(){
    }

    @When("I Click Button Sign in")
    public void whenIClickButtonSignIn(){  }

    @When("Page is direct to page input email address")
    public void whenPageisDirectPageInputemailAddress(){  }

    @When("I input email address")
    public void whenInputEmail(){   }

    @When("I Click Button Create an account")
    public void whenClikcButtonCreateAccount(){  }

    @Then("I Success Registration")
    public void ThenSuccessRegistration(){  }

}
