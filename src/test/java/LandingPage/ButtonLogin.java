package LandingPage;

import LandingPage.Steps.ButtonLoginSteps;
import net.serenitybdd.jbehave.SerenityStory;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class ButtonLogin extends SerenityStory {
    @Steps
    ButtonLoginSteps ButtonLoginStep;

    //===Succes Button Login Landing Page=====

    @Given("I Access home Page")
    public void givenIAccessHomePage(){
        ButtonLoginStep.checkHomePageInstagram();
    }

    @When("I Click Button Sign In")
    public void whenIClickButtonSignIn(){ ButtonLoginStep.clickButtonSignInHome(); }

    @When("Page is direct to page input username and password")
    public void whenPageisDirectPageInputUserAndPass(){ ButtonLoginStep.HomePageLoginIsDisplayed(); }

    @When("I input valid username and password")
    public void whenInputValidUserAndPass(){ ButtonLoginStep.InputUserAndPassword();  }

    @When("i Click Button Sign In")
    public void whenClikcButtonSignIn(){ ButtonLoginStep.clickButtonSignIn(); }

    @Then("I Success Sign In on the web")
    public void ThenSuccessSignIn(){ ButtonLoginStep.SuccessLoginInstagram(); }


}
