package LandingPage.Pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.awt.*;

public class ButtonLoginPages extends PageObject {

    //check Button Sign in

    @FindBy( xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a")
    WebElement ButtonSignInHome;

    public void clickButtonSignInHome(){
        ButtonSignInHome.click();
    }

    //check Text Area Email and password Page Login

    @FindBy(xpath = "//*[@id=\"email\"]")
    WebElement InputEmail;

    @FindBy(xpath = "//*[@id=\"passwd\"]")
    WebElement InputPassword;

    public void checkInputEmailAndPassword(){
    }

    //input username and password
    public void InputEmailAndPassword(String email, String Password){
        InputEmail.sendKeys(email);
        InputPassword.sendKeys(Password);
    }

    //click button Sign in

    @FindBy(xpath = "//*[@id=\"SubmitLogin\"]/span")
    WebElement ButtonSignIn;

    public void clickButtonSignIn(){
        element(ButtonSignIn).shouldBeEnabled();
        ButtonSignIn.click();
    }

}
