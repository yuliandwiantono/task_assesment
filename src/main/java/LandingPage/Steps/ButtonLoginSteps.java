package LandingPage.Steps;

import LandingPage.Pages.ButtonLoginPages;
import net.thucydides.core.steps.ScenarioSteps;
public class ButtonLoginSteps extends ScenarioSteps {

    ButtonLoginPages ButtonLoginPage;


    public void checkHomePageInstagram(){
        getDriver().manage().window().fullscreen();
        ButtonLoginPage.open();
    }


    public void clickButtonSignInHome() {
        ButtonLoginPage.clickButtonSignInHome();

    }

    public void HomePageLoginIsDisplayed(){
        ButtonLoginPage.checkInputEmailAndPassword();
    }

    public void InputUserAndPassword(){
        ButtonLoginPage.InputEmailAndPassword("nton.moz4@gmail.com","password123");
    }

    public void clickButtonSignIn(){
        ButtonLoginPage.clickButtonSignIn();
    }

    public void SuccessLoginInstagram(){

    }
}
